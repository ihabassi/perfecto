// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
    mode: "development",
    context: __dirname + '/src',
    entry: './app.js',
    output: {
        filename: 'bundle.js'
    },
    // plugins: [
    //     new MiniCssExtractPlugin({
    //         // Options similar to the same options in webpackOptions.output
    //         // both options are optional
    //         filename: devMode ? '[name].css' : '[name].[hash].css',
    //         chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    //     })
    // ],
    // plugins: [
    //     new MiniCssExtractPlugin({
    //         // Options similar to the same options in webpackOptions.output
    //         // both options are optional
    //         filename: "[name].css",
    //         chunkFilename: "[id].css"
    //     })
    // ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": ["env"]
                    }
                }
            },
            // {
            //     test: /\.(sa|sc|c)ss$/,
            //     use: [
            //         devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
            //         'css-loader',
            //         'postcss-loader',
            //         'sass-loader',
            //     ],
            // }
            {
                test: /\.css$/,
                use: [
                  // MiniCssExtractPlugin.loader,
                  'style-loader',
                  'css-loader'
                ]
            },
            // {
            //     test: /\.scss$/,
            //     use: [
            //         MiniCssExtractPlugin.loader,
            //         'css-loader',
            //         {
            //             loader: 'postcss-loader',
            //             options: {
            //                 plugins: () => [
            //                     require('autoprefixer')
            //                 ],
            //             }
            //         },
            //         {
            //             loader: 'sass-loader',
            //         }
            //     ]
            // }
        ]
    }
};