### Install Dependencies

I have preconfigured `npm` to automatically install webpack and it's dependencies, so to install the application just run:

```
npm install
```

### Run the Application

I have preconfigured the project with a webpack development web server, so to run the application just run:

```
npm start
```

Now browse to the app at [`localhost:8080`][local-app-url].