'use strict';

angular.module('myApp.musicDetails', [])
.directive('musicDetails', ['myService', function(myService){
    const directiveCTRL = function () {
        let dm = this;
        dm.myService = myService;
    };

    return {
        restrict: 'E',
        templateUrl: './directives/music-details/music-details.html',
        controller: directiveCTRL,
        controllerAs: 'dm',
        scope: {
            selectRecord: '&'
        },
        bindToController: {
            musicRecord: '=',
            recordIndex: '@',
            selectedIndex: '@'
        }
    };
}]);