'use strict';

export default angular.module('myApp.musicList', [])
.directive('musicList', function(){
    const directiveCTRL = function () {
        let dm = this;
        dm.selectRecord = function (selectedIndex) {
            dm.selectedIndex = selectedIndex;
        };

        dm.greaterThan = function(prop, val){
            return function(item){
                return item[prop] > val;
            }
        }

        dm.betweenDates = function(prop, filterOptions){
            return function(item){
                const fromDate = filterOptions.fromDate ? filterOptions.fromDate : new Date(2000, 1, 1);
                const toDate = filterOptions.toDate ? filterOptions.toDate : new Date();
                const checkDate = new Date(item[prop]);
                // return true;
                return checkDate >= fromDate && checkDate <= toDate;
            }
        }
    };

    return {
        restrict: 'E',
        templateUrl: './directives/music-list/music-list.html',
        controller: directiveCTRL,
        controllerAs: 'dm',
        bindToController: {
            musicList: '=',
            selectedIndex: '=',
            filterOptions: '='
        }
    };
});