'use strict';

export default angular.module('myApp.services', [])
.factory('myService', ['$http', '$q', function ($http, $q) {
	let data;
    const searchData = function(searchText) {
		const apiUrl = 'https://api.mixcloud.com/search/?q=' + encode(searchText) + '&amp;type=cloudcast';
        return $http.get(apiUrl)
        .then(function(response) {
          data = response.data.data;
          return data;
        }, function(errResponse){
			console.error('Error while fetching data');
			return $q.reject(errResponse);
		});
    }

    const encode = function(unencoded) {
        return encodeURIComponent(unencoded).replace(/'/g,"%27").replace(/"/g,"%22");	
    }

    const decode = function(encoded) {
		const decoded = decodeURIComponent(encoded.replace(/\+/g,  " "));
        return decoded;
    }

	return {
		searchData: searchData,
        encode: encode,
        decode: decode
    };
}]);