'use strict';

export default angular.module('myApp.routeOne', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.when('/routeOne', {
    templateUrl: 'routeOne/routeOne.html',
    controller: 'routeOneCtrl'
  });
}])

.controller('routeOneCtrl', ['$scope', 'myService', function ($scope, myService) {
  $scope.filterOptions = {
    minListener: 0
  };
  $scope.search = function (textToSearch) {
    myService.searchData(textToSearch).then(function(response){
      $scope.musicList = response;
    });
  };

  // date pickers code (got it from the web)
  $scope.dateOptions = {
    formatYear: 'yyyy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(2000, 1, 1),
    startingDay: 1
  };

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.format = 'yyyy/MM/dd';

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  $scope.updateListenersFilter = function (minListener) {
    // check if input contains only numbers
    if (/^\d+$/.test(minListener)) {
      $scope.filterOptions.minListener = minListener;
    }
  };

  $scope.updateFromDateFilter = function (fromDate) {
    $scope.filterOptions.fromDate = fromDate;
  };

  $scope.updateToDateFilter = function (toDate) {
    $scope.filterOptions.toDate = toDate;
  };
}]);