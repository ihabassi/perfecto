'use strict';
import 'angular';
import 'angular-route';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'angular-ui-bootstrap';
// import accordion from 'angular-ui-bootstrap/src/accordion';
// import datepicker from 'angular-ui-bootstrap/src/datepicker';
import '../src/style.css';
import './app.services.js';
import './routeOne/routeOne.js';
import './directives/music-list/music-list.js';
import './directives/music-details/music-details.js';

angular.module('myApp', [
    'ngRoute',
    'myApp.routeOne',
    'myApp.services',
    'myApp.musicList',
    'myApp.musicDetails',
    'ui.bootstrap'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
  
    $routeProvider.otherwise({redirectTo: '/routeOne'});
}]);